const puppeteer = require('puppeteer')
const express = require("express")

const app = express()

app.get('*', (request, res)=>{
    printPDF().then(pdf => {
        res.set({ 'Content-Type': 'application/pdf', 'Content-Length': pdf.length})
        res.send(pdf)
    })
    .catch((error)=>{
        console.error(error)
    })
})


async function printPDF() {
  const browser = await puppeteer.launch({ headless: true, args: ['--no-sandbox'], });
  const page = await browser.newPage();
  await page.goto('https://google.com', {waitUntil: 'networkidle0'});
  const pdf = await page.pdf({ format: 'A4' });
 
  await browser.close();
  return pdf
}

app.listen(5000, ()=>{
    console.log("Server running on port 5000")
})